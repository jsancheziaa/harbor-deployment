# Harbor Deployment



## Prerequisites

Before you embark on this guide, here is what you need.

- An instance of Ubuntu 20.04 with SSH access
- A regular user configured with sudo privileges
- 4 CPU Core & 4 GB RAM.
- 50 GB of SSD or another storage technology connected



For Kubernetes option:


- Master Node:
    - An instance of Ubuntu 20.04 with SSH access
    - A regular user configured with sudo privileges
    - 4 CPU Core & 4 GB RAM.
    - 50 GB of SSD or another storage technology connected


- Worker Node:
    - An instance of Ubuntu 20.04 with SSH access
    - A regular user configured with sudo privileges
    - 4 CPU Core & 4 GB RAM.
    - 50 GB of SSD or another storage technology connected


Link to more instruction to deploy kubernetes: https://github.com/manuparra/orchestrator-at-spsrc

## Use Ansible
You can install a released version of Ansible with pip or a package manager. See our installation guide for details on installing Ansible on a variety of platforms.

Power users and developers can run the devel branch, which has the latest features and fixes, directly. Although it is reasonably stable, you are more likely to encounter breaking changes when running the devel branch. We recommend getting involved in the Ansible community if you want to run the devel branch.


## Configuring Inventory File
You need to add the target host in the inventorie
```
vi inventories/host
```

## Harbor Config
You need to edit harbor.yml 

```
root@localhost:~/harbor# nano harbor.yml

# Configuration file of Harbor

# The IP address or hostname to access admin UI and registry service.
# DO NOT use localhost or 127.0.0.1, because Harbor needs to be accessed by external clients.
hostname: harbor.example.com

# http related config
http:
# port for http, default is 80. If https enabled, this port will redirect to https port
port: 80

# https related config
#https:
# https port for harbor, default is 443
# port: 443
# The path of cert and key files for nginx
# certificate: /your/certificate/path
# private_key: /your/private/key/path

# # Uncomment following will enable tls communication between all harbor components
# internal_tls:
# # set enabled to true means internal tls is enabled
# enabled: true
# # put your cert and key files on dir
# dir: /etc/harbor/tls/internal

# Uncomment external_url if you want to enable external proxy
# And when it enabled the hostname will no longer used
# external_url: https://reg.mydomain.com:8433

# The initial password of Harbor admin
# It only works in first time to install harbor
# Remember Change the admin password from UI after launching Harbor.
harbor_admin_password: Harbor12345
```

## Options
you have write diferent version for install Harbor:
1. Only Harbor (you need have installed and running Docker)
2. Docker and Harbor (this deploy include docker installations and Harbor)
3. Kurbenetes.

## Run Playbook
If you need install only harbor, your option is:
ansible-playbook -i inventories/host harbor.yml

If you need install docker and harbor toguether, your option is:
ansible-playbook -i inventories/host docker_and_harbor.yml

If you need to deploy Kubernetes and install Harbor in HA your option is:

ansible-playbook -i inventories/host deploy-kubernetes.yml
ansible-playbook -i inventories/host deploy-aplication.yml

The kubernetes option need to configure the values.yaml file with the parameters that you need or you can uses the command line to do this. We uses the option to pass the parameter in line.
```
helm install --wait <RELEASENAME> --namespace <NAMESPACE> --create-namespace --namespace <NAMESPACE>  harbor/harbor \
--set expose.type= <redacted> \ 
--set expose.nodePort.ports.http.nodePort=<redacted> \
--set expose.nodePort.ports.https.nodePort=<redacted> \
--set expose.nodePort.ports.notary.nodePort=<redacted> \
--set expose.tls.enable=true \
--set expose.tls.certSource=auto \
--set expose.tls.auto.commonName="<MASTER-NODE-IP>" \
--set expose.tls.secret.secretName="<SECRET>" \
--set persistence.enabled=false \
--set externalURL="https://ip_or_name:port" \
--set harborAdminPassword="<redacted>"  
```

```
helm install --wait rs-harbor-oidc --namespace ns-harbor-oidc --create-namespace --namespace ns-harbor-oidc  harbor/harbor \
--set expose.type=nodePort \
--set expose.nodePort.ports.http.nodePort=30040 \
--set expose.nodePort.ports.https.nodePort=30041 \
--set expose.nodePort.ports.notary.nodePort=30042 \
--set expose.tls.enable=true \
--set expose.tls.certSource=auto \
--set expose.tls.auto.commonName="k8s-master-dev" \
--set expose.tls.secret.secretName="k8s-master-dev" \
--set persistence.enabled=false \
--set externalURL="https://192.168.250.66:30041" \
--set harborAdminPassword="Harbor12345" 
```
Description of parameters of value.yaml
https://goharbor.io/docs/2.6.0/install-config/configure-yml-file/

## Open Harbor UI
After successful installation, you can go to your favorite website and open your local Server URL http://harbor.example.com as shown below. 
Once opened, it will ask you to put Username and Password to login. Your default username will be admin and password will be the same as you have set in the earlier step i.e Harbor12345.
